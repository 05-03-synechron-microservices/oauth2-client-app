package com.classpath.oauth2clientapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/userinfo")
@RequiredArgsConstructor
public class UserInfoRestController {

    private final OAuth2AuthorizedClientService oAuth2AuthorizedClientService;

    @GetMapping
    public Map<String, String> getUserInfo(OAuth2AuthenticationToken oAuth2AuthenticationToken){

        OAuth2AuthorizedClient oAuth2AuthorizedClient = this.oAuth2AuthorizedClientService
                .loadAuthorizedClient(oAuth2AuthenticationToken.getAuthorizedClientRegistrationId(), oAuth2AuthenticationToken.getPrincipal().getName());

        OAuth2AccessToken accessToken = oAuth2AuthorizedClient.getAccessToken();
        OAuth2RefreshToken refreshToken = oAuth2AuthorizedClient.getRefreshToken();
        //System.out.println("Refresh token :: "+ refreshToken.getTokenValue());
        String accessTokenTokenValue = accessToken.getTokenValue();
        Set<String> scopes = accessToken.getScopes();
        String issuedAt = accessToken.getIssuedAt().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE_TIME);
        String expiresAt = accessToken.getExpiresAt().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE_TIME);

        Map<String, String> resultMap = new LinkedHashMap<>();
        resultMap.put("access-token", accessTokenTokenValue);
        //resultMap.put("refresh-token", refreshToken.getTokenValue());
        resultMap.put("scopes", scopes.toString());
        resultMap.put("issued-at", issuedAt);
        resultMap.put("expirte-at", expiresAt);

        return resultMap;
    }
}
